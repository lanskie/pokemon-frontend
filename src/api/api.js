import axios from 'axios'
const API_ENDPOINT = 'http://localhost:8000';

export const login = (loginCred) => {
    return new Promise ((resolve, reject) => {
        axios.post(`${API_ENDPOINT}/login`, loginCred)
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
        })
    })
}

/**
 * @param {*} id - if not provided return own information
 * @returns Promise
 */
export const getUserById = (id) => {
    let user = localStorage.getItem("user");
    let parsed = JSON.parse(user);
    let userId = id ? id : parsed.id;
    return new Promise ((resolve, reject) => {
        axios.get(`${API_ENDPOINT}/users/${userId}`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const deleteMyPokemonById = (id) => {
    return new Promise ((resolve, reject) => {
        axios.delete(`${API_ENDPOINT}/my-pokemon/${id}`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const addPokemonLikeDislike = (data) => {
    return new Promise ((resolve, reject) => {
        axios.post(`${API_ENDPOINT}/my-pokemon/add-like`,
        data,{
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        }).then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const register = (data) => {
    return new Promise ((resolve, reject) => {
        axios.post(`${API_ENDPOINT}/register`, data)
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const update = (data) => {
    return new Promise ((resolve, reject) => {
        axios.post(`${API_ENDPOINT}/update`, 
        data,{
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
        })
    })
}

export const getUsers = () => {
    return new Promise ((resolve, reject) => {
        axios.get(`${API_ENDPOINT}/users`, {
            headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
        })
    })
}




