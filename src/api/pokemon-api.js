import axios from 'axios'
const API_ENDPOINT = 'https://pokeapi.co/api/v2';

export const getPockemonList = (limit, offset) => {
    return new Promise ((resolve, reject) => {
        axios.get(`${API_ENDPOINT}/pokemon-form?limit=${limit}&offset=${offset}`)
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
            console.error(error)
        })
    })
}


export const getPockemonByName = (name) => {
    return new Promise ((resolve, reject) => {
        axios.get(`${API_ENDPOINT}/pokemon/${name}`)
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
            console.error(error)
        })
    })
}

export const getAllItems = (offset, limit) => {
    return new Promise ((resolve, reject) => {
        axios.get(`${API_ENDPOINT}/item?offset=${offset}&limit=${limit}`)
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
            console.error(error)
        })
    })
}

export const getAllTypes = () => {
    return new Promise ((resolve, reject) => {
        axios.get(`${API_ENDPOINT}/type`)
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
            console.error(error)
        })
    })
}

export const getByTypes = (type) => {
    return new Promise ((resolve, reject) => {
        axios.get(`${API_ENDPOINT}/type/${type}`)
        .then((response) => {
            resolve(response.data)
        }).catch((error) => {
            reject(error)
            console.error(error)
        })
    })
}

