import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Pokedex from '../views/Pokedex'
import MyInfo from '../views/MyInfo'
import OtherUser from '../views/OtherUser'
import Items from '../views/Items'

Vue.use(VueRouter)


const routeGuards = (to, from, next) => {
  const isAuthenticated = typeof localStorage.getItem('token') === 'string'
  if (to.name !== 'Login' && !isAuthenticated) next({ name: 'Login' })
  else next()
}

const loginGuard = (to, from, next) => {
  console.log(to.name === 'Login' && isAuthenticated)
  const isAuthenticated = typeof localStorage.getItem('token') === 'string'
  if (to.name === 'Login' && isAuthenticated) next({ name: 'Pokedex' })
  else if (to.name === 'Register' && isAuthenticated) next({ name: 'Pokedex' })
  else next()
}

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    beforeEnter: loginGuard
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Registration.vue'),
    beforeEnter: loginGuard
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    children: [
      {
        path: '/',
        component: OtherUser,
        beforeEnter: routeGuards
      },
      {
        path: 'pokedex',
        name: 'Pokedex',
        component: Pokedex,
        beforeEnter: routeGuards
      },
      {
        path: 'items',
        name: 'Items',
        component: Items,
        beforeEnter: routeGuards
      },
      {
        path: 'my-pokemon',
        component: MyInfo,
        beforeEnter: routeGuards
      },
      {
        path: 'other-user',
        component: OtherUser,
        beforeEnter: routeGuards
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})



export default router
